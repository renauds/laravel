<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Painting extends Model
{
    use HasFactory;
    public function categorie()
    {
        return $this->belongsTo(Categorie::class);

    }
    public function techniques()
    {
        return $this->belongsTo(Technique::class);

    }
    protected $fillable = [
        'tech_id',
        'cat_id',
        'titre',
        'description',
        'hauteur',
        'largeur',
        'slug',
        'imageName',
        'date_peinture',
        'enregistrement',
    ];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'titre'
            ]
        ];
    }
}
