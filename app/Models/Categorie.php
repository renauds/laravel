<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{

    use HasFactory;
    public function painting()
    {
        return $this->hasMany(Painting::class);
    }
    protected $fillable = [
        'name',
        ];
}
