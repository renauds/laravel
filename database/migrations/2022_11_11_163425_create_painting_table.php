<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paintings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tech_id')->constrained('techniques');
            $table->foreignId('cat_id')->constrained('categories');
            $table->string('titre');
            $table->text('description');
            $table->float('hauteur');
            $table->float('largeur');
            $table->string('slug');
            $table->text('imageName');
            $table->date('date_peinture');
            $table->dateTime('enregistrement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paintings');
    }
};
