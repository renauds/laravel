<?php

namespace Database\Factories;

use Faker\Generator as Faker;
use App\Http\Models\Categorie;
use App\Http\Models\Technique;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Painting>
 */
class PaintingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = fake()->unique()->sentence(3);
        $slug = str_slug($title, '-');

        return [
            'tech_id' => factory(\App\Models\Technique::class)->create()->id,
            'cat_id' => factory(\App\Models\Categorie::class)->create()->id,
            'titre' => $title,
            'description' => fake()->paragraphs(2),
            'hauteur' => fake()->numberBetween(30,150),
            'largeur' => fake()->numberBetween(30,150),
            'slug' => $slug,
            'imageName' => '01.jpg',
            'date_peinture' => fake()->date('Y-m-d'),
            'enregistrement' =>now(),
        ];
    }
}
