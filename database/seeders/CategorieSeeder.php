<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { $categories=['abstrait','classisisme', 'cubisme','expressionisme', 'pointillisme','surealisme'];

            foreach ($categories as $category) {

                DB::table('categories')->insert([
                    'name' => $category,

                ]);

            }


    }
}
