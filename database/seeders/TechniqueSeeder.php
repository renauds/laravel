<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class TechniqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $techniques=['aquarelle', 'gouache','huile', 'laque','dripping'];

        foreach ($techniques as $technique) {

            DB::table('techniques')->insert([
                'name' => $technique,

            ]);

        }

    }
}
