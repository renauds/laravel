<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Database\Seeder;

class PaintingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i<=24; $i++){
            $categories= array('abstrait','classisisme', 'cubisme','expressionisme', 'pointillisme','surealisme');

            $techniques= array('aquarelle', 'gouache','huile', 'laque','dripping');
            $title = fake()->words(3,true);
            $slug = Str::slug($title,'-') ;
            DB::table('paintings')->insert([
                'tech_id' => fake()->numberBetween(1,sizeof($techniques)),
                'cat_id' => fake()->numberBetween(1,sizeof($categories)),
                'titre' => $title,
                'description' => fake()->paragraphs(2,true),
                'hauteur' => fake()->numberBetween(30,150),
                'largeur' => fake()->numberBetween(30,150),
                'slug' => $slug,
                'imageName' => $i.'.jpg',
                'date_peinture' => fake()->unique()->dateTimeBetween('-400 years','now'),
                'enregistrement' =>now(),

            ]);
        }
    }
}
